package com.glearning;

public class MergeSort {

	// dividing the problem statement to reduce
	public void sort(int[] input, int left, int right) {
		if (left < right) {

			// find out the mid element
			// left + (right - left)/2;
			// 0 + 20-0/2
			int mid = (left + right) / 2;
			//dividing the left sub array
			sort(input, left, mid);
			
			//dividing the right sub array
			sort(input, mid + 1, right);
			
			//merge the sorted arrays
			merge(input, left, mid, right);
		}
	}

	private void merge(int[] input, int left, int mid, int right) {
		
		//find the size of both the sub arrays to be merged
		int n1 = mid - left + 1;
		int n2 = right - mid;
		
		//create the two temporary arrays
		//the elements of the integer arrays with be filled with the default values
		/*
		 * int - 0
		 * boolean = false
		 * object = null
		 */
		int leftArray[] = new int[n1];
		int rightArray[] = new int[n2];
		
		//copy the elements to the temp array
		for(int i = 0; i < n1; ++i) {
			leftArray[i] = input[left + i];
		}
		
		for(int j = 0; j < n2; ++j) {
			rightArray[j] = input[mid + 1 + j];
		}
		
		//merging the sub arrays
		int i = 0;
		int j = 0;
		
		int current = 0;
		
		while(i < n1 && j < n2) {
			if(leftArray[i] <= rightArray[j]) {
				input[current] = leftArray[i];
				i++;
			} else {
				input[current] = rightArray[j];
				j++;
			}
			current ++;
		}
		//copy the remaining elements
		while(i < n1) {
			input[current] = leftArray[i];
			i++;
			current++;
		}
		

		//copy the remaining elements
		while(j < n2) {
			input[current] = rightArray[j];
			j++;
			current++;
		}
	}

}
