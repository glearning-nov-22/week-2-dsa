package com.glearning;

public class Client {
	
	public static void main(String[] args) {
		
		int[] input = new int[] { 3, 4, 2, 1, 9, 8, 7};
		
		MergeSort mergeSort = new MergeSort();

		//input array, left, right
		mergeSort.sort(input, 0, input.length - 1);
		
		//the array after sorting is
		for(int index = 0; index < input.length; index ++) {
			System.out.println(input[index]);
		}
		
	}

}
