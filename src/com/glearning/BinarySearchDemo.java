package com.glearning;

public class BinarySearchDemo {

	public static void main(String[] args) {
		int[] sortedArray = new int[] { 3, 4, 5, 8, 9, 10, 23, 25 };
		
		int key = 18;
		int end = sortedArray.length - 1;
		
		binarySearch(sortedArray, 0, end , key);
	}

	private static void binarySearch(int[] array, int start, int end, int key) {
		
		while(start <= end) {
			//check if the mid elemet is the key
			int mid = (start + end) / 2;
			if (array[mid] < key) {
				start = mid + 1;
			} else if( array[mid] > key) {
				end = mid - 1;
			} else {
				System.out.println("Element found :: "+ mid);
				break;
			}
		}
		
		if (start > end) {
			System.out.println("Element not found :: "+ key);
		}
		
		
	}

}
