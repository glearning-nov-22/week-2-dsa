package com.glearning;

public class LeftRotate {
	
	public static void main(String[] args) {
		int[] array = {1,2,3,4,7,8,9};
		
		int midElement = array.length / 2;
		
		leftRotate(array, midElement, array.length);
		
	}

	private static void leftRotate(int[] array, int element, int length) {
		for(int index = 0; index < element; index ++) {
			leftRotateByOne(array, length);
		}
		
	}

	private static void leftRotateByOne(int[] array, int length) {
		int i, temp;
		temp = array[0];
		
		for( i = 0; i < length -1 ; i++) {
			array[i] = array[i + 1];
		}
		array[length -1] = temp;
	}

}
